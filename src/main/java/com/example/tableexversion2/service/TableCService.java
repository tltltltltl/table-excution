package com.example.tableexversion2.service;

import java.util.UUID;

import com.example.tableexversion2.entity.ParamA;
import com.example.tableexversion2.entity.TableA;
import com.example.tableexversion2.entity.TableB;
import com.example.tableexversion2.entity.TableC;
import com.example.tableexversion2.mapper.ExcelTableAMapper;
import com.example.tableexversion2.mapper.ExcelTableBMapper;
import com.example.tableexversion2.mapper.ExcelTableCMapper;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TableCService {
    @Resource
    private ExcelTableAMapper extMapA;
    @Resource
    private ExcelTableBMapper extMapB;
    @Resource
    private ExcelTableCMapper extMapC;

    /*
    表a中需要的数据：
    aa,bb,cc,dd,ee
    表b中需要的数据：
    correct_aa, correct_bb,correct_cc,aa,bb,cc,correct_ee,correct_dd,
     */
    public void calculateForTableC() {
        //List<TableC> tableCList = extMapC.findAll();
        List<TableC> infoList = new ArrayList<>();
        Map<String, Integer> sumInTableA = new HashMap<>();
        Map<String, Integer> sumInTableB = new HashMap<>();
        List<TableA>  tableAList = extMapA.getFields();
        for (TableA tableA : tableAList) {
            sumInTableA.put("aa", sumInTableA.getOrDefault("aa",0) + tableA.getAa());
            sumInTableA.put("bb", sumInTableA.getOrDefault("bb",0) + tableA.getBb());
            sumInTableA.put("cc", sumInTableA.getOrDefault("cc",0) + tableA.getCc());
            sumInTableA.put("dd", sumInTableA.getOrDefault("dd",0) + tableA.getDd());
            sumInTableA.put("ee", sumInTableA.getOrDefault("ee",0) + tableA.getEe());
        }
        List<TableB> tableBList = extMapB.getFields();
        for(TableB tableB : tableBList) {
            sumInTableB.put("aa",sumInTableB.getOrDefault("aa", 0) + tableB.getAa());
            sumInTableB.put("bb",sumInTableB.getOrDefault("bb", 0) + tableB.getBb());
            sumInTableB.put("cc",sumInTableB.getOrDefault("cc", 0) + tableB.getCc());
            sumInTableB.put("correct_aa", sumInTableB.getOrDefault("correct_aa", 0) + tableB.getCorrect_aa());
            sumInTableB.put("correct_bb", sumInTableB.getOrDefault("correct_bb", 0) + tableB.getCorrect_bb());
            sumInTableB.put("correct_cc", sumInTableB.getOrDefault("correct_cc", 0) + tableB.getCorrect_cc());
            sumInTableB.put("correct_dd", sumInTableB.getOrDefault("correct_dd", 0) + tableB.getCorrect_dd());
            sumInTableB.put("correct_ee", sumInTableB.getOrDefault("correct_ee", 0) + tableB.getCorrect_ee());
        }
        float aas = sumInTableB.getOrDefault("correct_aa", 0) / (float) sumInTableA.getOrDefault("aa", 1);
        float bbs = sumInTableB.getOrDefault("correct_bb", 0) / (float) sumInTableA.getOrDefault("bb", 1);
        float ccs = sumInTableB.getOrDefault("correct_cc", 0) / (float) sumInTableA.getOrDefault("cc", 1);
        float aaa = sumInTableB.getOrDefault("correct_aa", 0) / (float) sumInTableB.getOrDefault("aa", 1);
        float bba = sumInTableB.getOrDefault("correct_bb", 0) / (float) sumInTableB.getOrDefault("bb", 1);
        float cca = sumInTableB.getOrDefault("correct_cc", 0) / (float) sumInTableB.getOrDefault("cc", 1);
        float aass = sumInTableB.getOrDefault("correct_ee", 0) / (float) sumInTableA.getOrDefault("ee", 1);
        float aac = (sumInTableB.getOrDefault("correct_aa", 0) + sumInTableB.getOrDefault("correct_ee", 0))
                / (float) (sumInTableA.getOrDefault("aa", 1) + sumInTableA.getOrDefault("ee", 1));
        float dds = sumInTableB.getOrDefault("correct_dd", 0) / (float) sumInTableA.getOrDefault("dd", 1);

        TableC tableC = new TableC(1L,"A",aas,bbs,ccs,aaa,bba,cca,aass,aac,dds);

        extMapC.save(tableC);
        //for ()
    }

    public List<TableC> getTableC() {
        return extMapC.getTableC();
    }

    public List<TableC> searchByB(ParamA paramA) {
        return extMapC.searchByB(paramA);
    }
}
