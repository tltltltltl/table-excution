package com.example.tableexversion2.service;

import com.example.tableexversion2.entity.ParamA;
import com.example.tableexversion2.entity.TableA;
import com.example.tableexversion2.mapper.ExcelTableAMapper;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TableAService {
    @Resource
    private ExcelTableAMapper extMap;

    public List<TableA> getTableA() {
        return extMap.getTableA();
    }

    public List<TableA> findBySearch(ParamA paramA) {
        return extMap.findBySearch(paramA);
    }

    public void addExcelA(TableA tableA) {
        extMap.addExcelA(tableA);
    }

    public void updateExcelA(TableA tableA) {
        extMap.updateExcelA(tableA);
    }


    public void deleteExcelA(ParamA paramA) {
        extMap.deleteExcelA(paramA);
    }
}
