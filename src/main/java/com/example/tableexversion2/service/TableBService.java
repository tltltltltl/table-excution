package com.example.tableexversion2.service;

import cn.hutool.core.io.IoUtil;
import cn.hutool.poi.excel.ExcelUtil;
import cn.hutool.poi.excel.ExcelWriter;
import com.example.tableexversion2.common.Result;
import com.example.tableexversion2.entity.ParamA;
import com.example.tableexversion2.entity.TableA;
import com.example.tableexversion2.entity.TableB;
import com.example.tableexversion2.mapper.ExcelTableBMapper;
import jakarta.annotation.Resource;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TableBService {
    @Resource
    private ExcelTableBMapper extMap;
    //get all
    public List<TableB> getTableB() {
        return extMap.getTableB();
    }

    public void addExcelB(TableB tableB) {
        extMap.addExcelB(tableB);
    }

    public void updateExcelB(TableB tableB) {
        extMap.updateExcel(tableB);
    }


    public void deleteExcelByC(ParamA paramA) {
        extMap.deleteExcelByC(paramA);
    }

    public void deleteExcelB(ParamA paramA) {
        extMap.deleteExcelB(paramA);
    }

}
