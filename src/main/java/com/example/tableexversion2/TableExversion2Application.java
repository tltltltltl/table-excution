package com.example.tableexversion2;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.example.tableexversion2.mapper")
public class TableExversion2Application {

    public static void main(String[] args) {
        SpringApplication.run(TableExversion2Application.class, args);
    }

}
