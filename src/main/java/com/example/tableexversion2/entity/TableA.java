package com.example.tableexversion2.entity;

import cn.hutool.core.annotation.Alias;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


@Table(name = "table_a")
public class TableA {
    //可写自增
    @Id
    @Column(name = "A",table="table_a")
    private String A;
    @Column(name = "B",table="table_a")
    private String B;
    @Column(name = "C",table="table_a")
    private String C;
    @Column(name = "D",table="table_a")
    private String D;
    @Column(name = "E",table="table_a")
    private String E;
    @Column(name = "F",table="table_a")
    private String F;
    @Column(name = "aa",table="table_a")
    private Integer aa;
    @Column(name = "bb",table="table_a")
    private Integer bb;
    @Column(name = "cc",table="table_a")
    private Integer cc;
    @Column(name = "dd",table="table_a")
    private Integer dd;
    @Column(name = "ee",table="table_a")
    private Integer ee;

    public TableA() {}
    public TableA(String a, String b, String c, String d, String e, String f, Integer aa, Integer bb, Integer cc, Integer dd, Integer ee) {
        A = a;
        B = b;
        C = c;
        D = d;
        E = e;
        F = f;
        this.aa = aa;
        this.bb = bb;
        this.cc = cc;
        this.dd = dd;
        this.ee = ee;
    }

    public String getA() {
        return A;
    }

    public void setA(String a) {
        A = a;
    }

    public String getB() {
        return B;
    }

    public void setB(String b) {
        B = b;
    }

    public String getC() {
        return C;
    }

    public void setC(String c) {
        C = c;
    }

    public String getD() {
        return D;
    }

    public void setD(String d) {
        D = d;
    }

    public String getE() {
        return E;
    }

    public void setE(String e) {
        E = e;
    }

    public String getF() {
        return F;
    }

    public void setF(String f) {
        F = f;
    }

    public Integer getAa() {
        return aa;
    }

    public void setAa(Integer aa) {
        this.aa = aa;
    }

    public Integer getBb() {
        return bb;
    }

    public void setBb(Integer bb) {
        this.bb = bb;
    }

    public Integer getCc() {
        return cc;
    }

    public void setCc(Integer cc) {
        this.cc = cc;
    }

    public Integer getDd() {
        return dd;
    }

    public void setDd(Integer dd) {
        this.dd = dd;
    }

    public Integer getEe() {
        return ee;
    }

    public void setEe(Integer ee) {
        this.ee = ee;
    }
}