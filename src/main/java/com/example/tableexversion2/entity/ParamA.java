package com.example.tableexversion2.entity;

//这个类主要是用来接受包装数据的
public class ParamA {
    //字段a模糊查询
    private String A;
    //字段b模糊查询
    private String B;
    //字段c 一键删除
    private String C;

    //用于C的计算操作

    public String getC() {
        return C;
    }

    public void setC(String c) {
        C = c;
    }

    public String getB() {
        return B;
    }

    public void setB(String b) {
        B = b;
    }

    public String getA() {
        return A;
    }

    public void setA(String a) {
        A = a;
    }
}
