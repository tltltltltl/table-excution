package com.example.tableexversion2.entity;

import cn.hutool.core.annotation.Alias;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;

@Table(name = "table_b")

public class TableB {
    @Column(name = "A",table="table_b")
    private String A;
    @Column(name = "B",table="table_b")
    private String B;
    @Column(name = "C",table="table_b")
    private String C;
    @Column(name = "D",table="table_b")
    private String D;
    @Column(name = "E",table="table_b")
    private String E;
    @Column(name = "F",table="table_b")
    private String F;
    @Column(name = "aa",table="table_b")
    private Integer aa;
    @Column(name = "correct_aa",table="table_b")
    private Integer correct_aa;
    @Column(name = "bb",table="table_b")
    private Integer bb;
    @Column(name = "correct_bb",table="table_b")
    private Integer correct_bb;
    @Column(name = "cc",table="table_b")
    private Integer cc;
    @Column(name = "correct_cc",table="table_b")
    private Integer correct_cc;
    @Column(name = "dd",table="table_b")
    private Integer dd;
    @Column(name = "correct_dd",table="table_b")
    private Integer correct_dd;
    @Column(name = "ee",table="table_b")
    private Integer ee;
    @Column(name = "correct_ee",table="table_b")
    private Integer correct_ee;

    public String getA() {
        return A;
    }

    public void setA(String a) {
        A = a;
    }

    public String getB() {
        return B;
    }

    public void setB(String b) {
        B = b;
    }

    public String getC() {
        return C;
    }

    public void setC(String c) {
        C = c;
    }

    public String getD() {
        return D;
    }

    public void setD(String d) {
        D = d;
    }

    public String getE() {
        return E;
    }

    public void setE(String e) {
        E = e;
    }

    public String getF() {
        return F;
    }

    public void setF(String f) {
        F = f;
    }

    public Integer getAa() {
        return aa;
    }

    public void setAa(Integer aa) {
        this.aa = aa;
    }

    public Integer getCorrect_aa() {
        return correct_aa;
    }

    public void setCorrect_aa(Integer correct_aa) {
        this.correct_aa = correct_aa;
    }

    public Integer getBb() {
        return bb == null ? 0 : bb;
    }

    public void setBb(Integer bb) {
        this.bb = bb;
    }

    public Integer getCorrect_bb() {
        return correct_bb;
    }

    public void setCorrect_bb(Integer correct_bb) {
        this.correct_bb = correct_bb;
    }

    public Integer getCc() {
        return cc;
    }

    public void setCc(Integer cc) {
        this.cc = cc;
    }

    public Integer getCorrect_cc() {
        return correct_cc;
    }

    public void setCorrect_cc(Integer correct_cc) {
        this.correct_cc = correct_cc;
    }

    public Integer getDd() {
        return dd;
    }

    public void setDd(Integer dd) {
        this.dd = dd;
    }

    public Integer getCorrect_dd() {
        return correct_dd;
    }

    public void setCorrect_dd(Integer correct_dd) {
        this.correct_dd = correct_dd;
    }

    public Integer getEe() {
        return ee;
    }

    public void setEe(Integer ee) {
        this.ee = ee;
    }

    public Integer getCorrect_ee() {
        return correct_ee == null ? 0 : correct_ee;
    }

    public void setCorrect_ee(Integer correct_ee) {
        this.correct_ee = correct_ee;
    }
}
