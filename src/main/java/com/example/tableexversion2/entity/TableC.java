package com.example.tableexversion2.entity;

import javax.persistence.*;

@Table(name = "table_c")
public class TableC {
    @Id
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id",table="table_c")
    private Long id;
    @Column(name = "B",table="table_c")
    private String B;
    @Column(name = "aaS",table="table_c")
    private Float aaS;
    @Column(name = "bbS",table="table_c")
    private Float bbS;
    @Column(name = "ccS",table="table_c")
    private Float ccS;
    @Column(name = "aaA",table="table_c")
    private Float aaA;
    @Column(name = "bbA",table="table_c")
    private Float bbA;
    @Column(name = "ccA",table="table_c")
    private Float ccA;
    @Column(name = "aaSS",table="table_c")
    private Float aaSS;
    @Column(name = "aaC",table="table_c")
    private Float aaC;
    @Column(name = "ddS",table="table_c")
    private Float ddS;

    public TableC() {
    }


    public TableC(Long id,String b, Float aaS, Float bbS, Float ccS, Float aaA, Float bbA, Float ccA, Float aaSS, Float aaC, Float ddS) {
        this.id = id;
        B = b;
        this.aaS = aaS;
        this.bbS = bbS;
        this.ccS = ccS;
        this.aaA = aaA;
        this.bbA = bbA;
        this.ccA = ccA;
        this.aaSS = aaSS;
        this.aaC = aaC;
        this.ddS = ddS;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getB() {
        return B;
    }

    public void setB(String b) {
        B = b;
    }

    public Float getAaS() {
        return aaS;
    }

    public void setAaS(Float aaS) {
        this.aaS = aaS;
    }

    public Float getBbS() {
        return bbS;
    }

    public void setBbS(Float bbS) {
        this.bbS = bbS;
    }

    public Float getCcS() {
        return ccS;
    }

    public void setCcS(Float ccS) {
        this.ccS = ccS;
    }

    public Float getAaA() {
        return aaA;
    }

    public void setAaA(Float aaA) {
        this.aaA = aaA;
    }

    public Float getBba() {
        return bbA;
    }

    public void setBba(Float bbA) {
        this.bbA = bbA;
    }

    public Float getCcA() {
        return ccA;
    }

    public void setCcA(Float ccA) {
        this.ccA = ccA;
    }

    public Float getAaSS() {
        return aaSS;
    }

    public void setAaSS(Float aaSS) {
        this.aaSS = aaSS;
    }

    public Float getAaC() {
        return aaC;
    }

    public void setAaC(Float aaC) {
        this.aaC = aaC;
    }

    public Float getDdS() {
        return ddS;
    }

    public void setDdS(Float ddS) {
        this.ddS = ddS;
    }
}
