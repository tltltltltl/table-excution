package com.example.tableexversion2.controller;

import cn.hutool.core.io.IoUtil;
import cn.hutool.poi.excel.ExcelUtil;
import cn.hutool.poi.excel.ExcelWriter;
import com.example.tableexversion2.common.Result;
import com.example.tableexversion2.entity.ParamA;
import com.example.tableexversion2.entity.TableA;
import com.example.tableexversion2.entity.TableB;
import com.example.tableexversion2.service.TableBService;
import jakarta.annotation.Resource;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin
@RestController
@RequestMapping("/tableB")
public class TableBController {
    @Resource
    private TableBService tableBService;

    @GetMapping
    public Result getAll() {
        List<TableB> list = tableBService.getTableB();
        return Result.success(list);
    }
    //新增
    @PostMapping("/add")
    public Result addExcelB(@RequestBody TableB tableB) {
        tableBService.addExcelB(tableB);
        return Result.success();
    }
    //编辑
    @PutMapping("/update")
    public Result updateExcelB(@RequestBody TableB tableB) {
        tableBService.updateExcelB(tableB);
        return Result.success();
    }
    //删除
    @DeleteMapping("/delete")
    public Result deleteExcelB(HttpServletRequest request) {
        String s = request.getParameter("paramA");
        ParamA paramA = new ParamA();
        paramA.setA(s);
        tableBService.deleteExcelB(paramA);
        return Result.success();
    }
    //按C删除
    @DeleteMapping("/delete/{valC}")
    public Result deleteExcelBByC(@PathVariable String valC) {
        ParamA paramA = new ParamA();
        paramA.setC(valC);
        tableBService.deleteExcelByC(paramA);
        return Result.success();
    }

    //导入
    @PostMapping("/upload")
    public Result uploadExcelA(MultipartFile file) throws IOException {
        List<TableB> infoList = ExcelUtil.getReader(file.getInputStream()).readAll(TableB.class);
        if (!CollectionUtils.isEmpty(infoList)) {
            for (TableB tableB : infoList) {
                try {
                    tableBService.addExcelB(tableB);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return Result.success();
    }

    //导出
    @GetMapping("/export")
    public Result exportExcel(HttpServletResponse response) throws IOException {
        //组装数据
        //1.查询所有数据
        List<TableB> all = tableBService.getTableB();
        if (CollectionUtils.isEmpty(all)) {
            return Result.error("未找到数据");
        }
        //2.定义List Map
        List<Map<String, Object>> list = new ArrayList<>(all.size());
        //3.遍历数据
        for (TableB tableB : all) {
            Map<String, Object> row = new HashMap<>();
            row.put("A",tableB.getA());
            row.put("B",tableB.getB());
            row.put("C",tableB.getC());
            row.put("D",tableB.getC());
            row.put("E",tableB.getE());
            row.put("F",tableB.getF());
            row.put("aa",tableB.getAa());
            row.put("correct_aa",tableB.getCorrect_aa());
            row.put("bb",tableB.getBb());
            row.put("correct_bb",tableB.getCorrect_bb());
            row.put("cc",tableB.getCc());
            row.put("correct_cc",tableB.getCorrect_cc());
            row.put("dd",tableB.getDd());
            row.put("correct_dd",tableB.getCorrect_dd());
            row.put("ee",tableB.getEe());
            row.put("correct_ee",tableB.getCorrect_ee());
            list.add(row);
        }
        //创建ExcelWriter
        ExcelWriter writer = ExcelUtil.getWriter(true);
        writer.write(list,true);
        //下载Excel
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setHeader("Content-disposition", "attachment;filename=tableB.xlsx");
        ServletOutputStream out = response.getOutputStream();
        writer.flush(out);
        writer.close();
        IoUtil.close(System.out);
        return Result.success("导出成功");
    }
}
