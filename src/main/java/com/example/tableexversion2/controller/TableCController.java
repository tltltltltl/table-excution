package com.example.tableexversion2.controller;

import cn.hutool.core.io.IoUtil;
import cn.hutool.poi.excel.ExcelUtil;
import cn.hutool.poi.excel.ExcelWriter;
import com.example.tableexversion2.common.Result;
import com.example.tableexversion2.entity.ParamA;
import com.example.tableexversion2.entity.TableB;
import com.example.tableexversion2.entity.TableC;
import com.example.tableexversion2.service.TableCService;
import jakarta.annotation.Resource;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin
@RestController
@RequestMapping("/tableC")
public class TableCController {

    @Resource
    private TableCService tableCService;
    //模糊查询
    @GetMapping("/search")
    public Result findBySearch(HttpServletRequest request) {
        String s = request.getParameter("paramA");
        ParamA paramA = new ParamA();
        paramA.setB(s);
        List<TableC> list = tableCService.searchByB(paramA);
        return Result.success(list);
    }
    //导出
    @GetMapping("/export")
    public Result exportExcel(HttpServletResponse response) throws IOException {
        //组装数据
        //1.查询所有数据
        List<TableC> all = tableCService.getTableC();
        if (CollectionUtils.isEmpty(all)) {
            return Result.error("未找到数据");
        }
        //2.定义List Map
        List<Map<String, Object>> list = new ArrayList<>(all.size());
        //3.遍历数据
        for (TableC tableC : all) {
            Map<String, Object> row = new HashMap<>();
            row.put("id",tableC.getId());
            row.put("B", tableC.getB());
            row.put("aaS", tableC.getAaS());
            row.put("bbS", tableC.getBbS());
            row.put("ccS", tableC.getCcS());
            row.put("aaA", tableC.getAaA());
            row.put("bbA", tableC.getBba());
            row.put("ccA", tableC.getCcA());
            row.put("aaSS", tableC.getAaSS());
            row.put("aaC", tableC.getAaC());
            row.put("ddS", tableC.getDdS());
            list.add(row);
        }
        //创建ExcelWriter
        ExcelWriter writer = ExcelUtil.getWriter(true);
        writer.write(list,true);
        //下载Excel
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setHeader("Content-disposition", "attachment;filename=tableB.xlsx");
        ServletOutputStream out = response.getOutputStream();
        writer.flush(out);
        writer.close();
        IoUtil.close(System.out);
        return Result.success("导出成功");
    }
    //一键计算
    @PostMapping("/calculate")
    public Result calculateForTableC() {
        tableCService.calculateForTableC();
        return Result.success();
    }
}
