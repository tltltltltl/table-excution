package com.example.tableexversion2.controller;

import cn.hutool.poi.excel.ExcelUtil;
import com.example.tableexversion2.common.Result;
import com.example.tableexversion2.entity.ParamA;
import com.example.tableexversion2.entity.TableA;
import com.example.tableexversion2.service.TableAService;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@CrossOrigin//解决跨域
@RestController
@RequestMapping("/tableA")
public class TableAController {
    @Resource
    private TableAService tableAService;
    @GetMapping
    public Result getAll() {
        List<TableA> list = tableAService.getTableA();
        return Result.success(list);
    }

    //模糊查询
    @GetMapping("/search")
    public Result findBySearch(HttpServletRequest request) {
        String s = request.getParameter("paramA");
        ParamA paramA = new ParamA();
        paramA.setA(s);
        List<TableA> list = tableAService.findBySearch(paramA);
        return  Result.success(list);
    }

    //新增
    @PostMapping("/add")
    public Result addExcelA(@RequestBody TableA tableA) {
        tableAService.addExcelA(tableA);
        return Result.success();
    }

    //编辑
    @PutMapping("/update")
    public Result updateExcelA(@RequestBody TableA tableA) {
        tableAService.updateExcelA(tableA);
        return Result.success();
    }

    //删除
    @DeleteMapping("/delete/{valA}")
    public Result deleteExcelA(@PathVariable String valA) {
        ParamA paramA = new ParamA();
        paramA.setA(valA);
        tableAService.deleteExcelA(paramA);
        return Result.success();
    }

    //导入
    @PostMapping("/upload")
    public Result uploadExcelA(MultipartFile file) throws IOException {
        List<TableA> infoList = ExcelUtil.getReader(file.getInputStream()).readAll(TableA.class);
        if (!CollectionUtils.isEmpty(infoList)) {
            for (TableA tableA : infoList) {
                try {
                    tableAService.addExcelA(tableA);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return Result.success();
    }
}
