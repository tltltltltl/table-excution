package com.example.tableexversion2.mapper;

import com.example.tableexversion2.entity.ParamA;
import com.example.tableexversion2.entity.TableA;
import com.example.tableexversion2.entity.TableB;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
//dao liked
public interface ExcelTableBMapper {
    List<TableB> getTableB();

    void addExcelB(TableB tableB);

    void updateExcel(TableB tableB);

    void deleteExcelB(@Param("paramA") ParamA paramA);

    void deleteExcelByC(@Param("paramA")ParamA paramA);

    List<TableB> getFields();

    //List<TableB> searchByC(String c);
}
