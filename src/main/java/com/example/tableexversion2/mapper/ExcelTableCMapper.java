package com.example.tableexversion2.mapper;

import com.example.tableexversion2.entity.ParamA;
import com.example.tableexversion2.entity.TableC;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ExcelTableCMapper {
    void save(TableC tableC);

    List<TableC> getTableC();

    List<TableC> searchByB(@Param("paramA") ParamA paramA);
}
