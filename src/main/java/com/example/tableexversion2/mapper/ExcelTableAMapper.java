package com.example.tableexversion2.mapper;

import com.example.tableexversion2.entity.ParamA;
import com.example.tableexversion2.entity.TableA;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@Repository
//dao liked
public interface ExcelTableAMapper extends Mapper<TableA> {
    //@Select("select * from table_a")
    List<TableA> getTableA();

    List<TableA> findBySearch(@Param("paramA") ParamA paramA);

    void addExcelA(TableA tableA);

    void deleteExcelA(@Param("paramA") ParamA paramA);

    void updateExcelA(TableA tableA);

    List<TableA> getFields();

    //

}
